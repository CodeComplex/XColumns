// ***********************************************************************
// Assembly         : XColumns
// Author           : Scott Michaels
// Created          : 04-01-2017
//
// Last Modified By : Scott Michaels
// Last Modified On : 04-01-2017
// ***********************************************************************
// <copyright file="SSMSInstance.cs" company="Code Crazy">
//     Copyright �  2017
// </copyright>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using XColumns.Annotations;

namespace XColumns
{
    /// <summary>
    /// Class SsmsInstance.
    /// </summary>
    /// <seealso cref="INotifyPropertyChanged" />
    public class SsmsInstance : INotifyPropertyChanged
    {
        /// <summary>
        /// The columns backing field.
        /// </summary>
        private ObservableCollection<SsmsColumn> _columns;
        /// <summary>
        /// Gets or sets the instance identifier.
        /// This is a randomly assigned UUID value.
        /// </summary>
        /// <value>The identifier.</value>
        public Guid Id { get; set; }
        /// <summary>
        /// Gets or sets the name of the instance.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the version of the instance.
        /// In this case, it's only a year. [2012, 2014, 2016, etc.)
        /// </summary>
        /// <value>The version.</value>
        public string Version { get; set; }
        /// <summary>
        /// Gets or sets the number.
        /// This is a number between 11 and 14. It is used by the registry access class.
        /// </summary>
        /// <value>The number.</value>
        public int Number { get; set; }
        /// <summary>
        /// Gets or sets the columns70.
        /// </summary>
        /// <value>The columns70.</value>
        public IEnumerable<SsmsColumn> Columns70 { get; set; }
        /// <summary>
        /// Gets or sets the columns80.
        /// </summary>
        /// <value>The columns80.</value>
        public IEnumerable<SsmsColumn> Columns80 { get; set; }

        /// <summary>
        /// Gets or sets the UI displayed columns.
        /// </summary>
        /// <value>The columns.</value>
        public ObservableCollection<SsmsColumn> Columns
        {
            get => _columns;
            set { _columns = value; OnPropertyChanged(); }
        }

        /// <summary>
        /// Occurs when a property value changes.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Called when a property has changed.
        /// </summary>
        /// <param name="propertyName">Name of the property.</param>
        [NotifyPropertyChangedInvocator]
        protected virtual void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}