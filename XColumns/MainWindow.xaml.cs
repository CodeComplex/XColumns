﻿// ***********************************************************************
// Assembly         : XColumns
// Author           : Scott Michaels
// Created          : 03-31-2017
//
// Last Modified By : Scott Michaels
// Last Modified On : 04-01-2017
// ***********************************************************************
// <copyright file="MainWindow.xaml.cs" company="Code Crazy">
//     Copyright ©  2017
// </copyright>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using System.Windows.Media.Animation;

namespace XColumns
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    /// <seealso cref="MahApps.Metro.Controls.MetroWindow" />
    /// <seealso cref="System.Windows.Markup.IComponentConnector" />
    public partial class MainWindow
    {
        /// <summary>
        /// Triggered when a column checkbox is checked or unchecked.
        /// </summary>
        public static RoutedCommand ToggleColumnCommand = new RoutedCommand("ToggleColumnCommand", typeof(MainWindow));

        /// <summary>
        /// The possible columns that can be shown in SSMS
        /// </summary>
        internal static readonly IEnumerable<KeyValuePair<int, string>> SsmsColumns = new List<KeyValuePair<int, string>>
        {
            new KeyValuePair<int, string>(1, "Column Name"),
            new KeyValuePair<int, string>(2, "Data Type"),
            new KeyValuePair<int, string>(3, "Length"),
            new KeyValuePair<int, string>(4, "Precision"),
            new KeyValuePair<int, string>(5, "Scale"),
            new KeyValuePair<int, string>(6, "Allow Nulls"),
            new KeyValuePair<int, string>(7, "Default Value"),
            new KeyValuePair<int, string>(8, "Identity"),
            new KeyValuePair<int, string>(10, "Identity Increment"),
            new KeyValuePair<int, string>(11, "Row GUID"),
            new KeyValuePair<int, string>(12, "Nullable"),
            new KeyValuePair<int, string>(13, "Condensed Type"),
            new KeyValuePair<int, string>(14, "Not for Replication"),
            new KeyValuePair<int, string>(15, "Formula"),
            new KeyValuePair<int, string>(16, "Collation"),
            new KeyValuePair<int, string>(17, "Description")
        };

        /// <summary>
        /// The SSMS instances found in the registry
        /// </summary>
        private SsmsInstance[] _instances;

        /// <summary>
        /// The SSMS tabs property
        /// </summary>
        public static readonly DependencyProperty SsmsTabsProperty = DependencyProperty.Register(
            "SsmsTabs", typeof(ObservableCollection<SsmsInstance>), typeof(MainWindow), new PropertyMetadata(new ObservableCollection<SsmsInstance>()));

        /// <summary>
        /// Gets or sets the tabs that are displayed in the UI.
        /// </summary>
        /// <value>The SSMS tabs.</value>
        public ObservableCollection<SsmsInstance> SsmsTabs
        {
            get { return (ObservableCollection<SsmsInstance>) GetValue(SsmsTabsProperty); }
            set { SetValue(SsmsTabsProperty, value); }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainWindow"/> class.
        /// </summary>
        public MainWindow()
        {
            InitializeComponent();

            SourceInitialized += MainWindow_SourceInitialized;
        }

        /// <summary>
        /// Handles the SourceInitialized event of the MainWindow control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void MainWindow_SourceInitialized(object sender, EventArgs e)
        {
            SourceInitialized -= MainWindow_SourceInitialized;

            CommandBindings.Add(new CommandBinding(ToggleColumnCommand, OnToggleColumnCommand));

            _instances = RegistryAccessor.GetSsmsData();

            foreach (var sd in _instances)
            {
                if (sd.Columns70 != null && sd.Columns70.Any())
                {
                    var xcols = new ObservableCollection<SsmsColumn>();
                    var columns = SsmsColumns.Select(c => new SsmsColumn { Name = c.Value, Number = c.Key, IsSelected = false }).ToList();
                    columns.ForEach(xcols.Add);

                    foreach (var c in columns)
                    {
                        c.InstanceId = sd.Id;

                        foreach (var cx in sd.Columns70)
                        {
                            if (c.Number == cx.Number)
                                c.IsSelected = true;
                        }
                    }

                    sd.Columns = xcols;
                }

                SsmsTabs.Add(sd);
            }

        }

        /// <summary>
        /// Handles the <see cref="E:ToggleColumnCommand" /> command.
        /// </summary>
        /// <param name="sender">The sender.</param>
        /// <param name="e">The <see cref="ExecutedRoutedEventArgs"/> instance containing the command data.</param>
        private void OnToggleColumnCommand(object sender, ExecutedRoutedEventArgs e)
        {
            var column = e.Parameter as SsmsColumn;
            if (column == null) return;

            var instance = _instances.First(x => x.Id == column.InstanceId);
            if (RegistryAccessor.UpdateSsmsInstance(instance.Number, instance.Columns))
            {
                var blinkAnimation = TryFindResource("blinkAnimation") as Storyboard;
                blinkAnimation?.Begin();
            }
        }
    }
}
