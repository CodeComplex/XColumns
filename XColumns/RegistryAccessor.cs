// ***********************************************************************
// Assembly         : XColumns
// Author           : Scott Michaels
// Created          : 04-01-2017
//
// Last Modified By : Scott Michaels
// Last Modified On : 04-01-2017
// ***********************************************************************
// <copyright file="RegistryAccessor.cs" company="Code Crazy">
//     Copyright �  2017
// </copyright>
// ***********************************************************************
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using Microsoft.Win32;

namespace XColumns
{
    /// <summary>
    /// Class RegistryAccessor.
    /// </summary>
    internal class RegistryAccessor
    {
        /// <summary>
        /// Updates the SSMS instance.
        /// </summary>
        /// <param name="vNumber">The version number of the instance to be updated.</param>
        /// <param name="columns">The columns that will be added to the registry.</param>
        /// <returns><c>true</c> if successfully updated, <c>false</c> otherwise.</returns>
        public static bool UpdateSsmsInstance(int vNumber, IEnumerable<SsmsColumn> columns)
        {
            var sb = new StringBuilder();
            foreach (var c in columns)
            {
                if (c.IsSelected)
                    sb.Append($"{c.Number},");
            }

            var cols = $"{sb.ToString().TrimEnd(',', ' ')};";
            var regPath = $"Software\\Microsoft\\SQL Server Management Studio\\{vNumber}.0\\DataProject";
            using (var rk = Registry.CurrentUser.OpenSubKey(regPath, true))
            {
                if (rk == null) return false;

                try
                {
                    rk.SetValue("SSVPropViewColumnsSQL70", cols, RegistryValueKind.String);
                    rk.SetValue("SSVPropViewColumnsSQL80", cols, RegistryValueKind.String);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e);
                    return false;
                }
            }

            return true;
        }

        /// <summary>
        /// Gets the SSMS data.
        /// </summary>
        /// <returns>SsmsInstance[].</returns>
        public static SsmsInstance[] GetSsmsData()
        {
            var lst = new List<SsmsInstance>();

            for (var i = 11; i < 14; i++)
            {
                var regPath = $"Software\\Microsoft\\SQL Server Management Studio\\{i}.0\\DataProject";
                var name = "SSMS ";

                if (i == 11)
                    name += "2012";
                else if (i == 12)
                    name += "2014";
                else if (i == 13)
                    name += "2016";

                var ssms = new SsmsInstance
                {
                    Name = name,
                    Version = $"{i}.0",
                    Id = Guid.NewGuid(),
                    Number = i,
                    Columns = new ObservableCollection<SsmsColumn>()
                };
                
                using (var rk = Registry.CurrentUser.OpenSubKey(regPath))
                {
                    if (rk == null)
                    {
                        lst.Add(ssms);
                        continue;
                    }

                    var rkVal = rk.GetValue("SSVPropViewColumnsSQL70", "");

                    if (!string.IsNullOrWhiteSpace(rkVal.ToString()))
                        ssms.Columns70 = ParseRegValue(rkVal.ToString());

                    rkVal = rk.GetValue("SSVPropViewColumnsSQL80", "");
                    if (!string.IsNullOrWhiteSpace(rkVal.ToString()))
                        ssms.Columns80 = ParseRegValue(rkVal.ToString());
                }   
                
                lst.Add(ssms);
            }

            return lst.ToArray();
        }

        /// <summary>
        /// Parses the reg value.
        /// </summary>
        /// <param name="regValue">The reg value.</param>
        /// <returns>IEnumerable&lt;SsmsColumn&gt;.</returns>
        private static IEnumerable<SsmsColumn> ParseRegValue(string regValue)
        {
            var vals = regValue.TrimEnd(';').Split(',');
            var ssms = new List<SsmsColumn>();

            foreach (var v in vals)
            {
                if (int.TryParse(v, out int x))
                {
                    var kvp = MainWindow.SsmsColumns.FirstOrDefault(c => c.Key == x);
                    ssms.Add(new SsmsColumn{Name = kvp.Value, Number = kvp.Key});
                }
            }
            return ssms;
        }
    }
}