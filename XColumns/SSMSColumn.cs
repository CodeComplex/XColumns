// ***********************************************************************
// Assembly         : XColumns
// Author           : Scott Michaels
// Created          : 04-01-2017
//
// Last Modified By : Scott Michaels
// Last Modified On : 04-01-2017
// ***********************************************************************
// <copyright file="SSMSColumn.cs" company="Code Crazy">
//     Copyright �  2017
// </copyright>
// ***********************************************************************
using System;

namespace XColumns
{
    /// <summary>
    /// Class SsmsColumn.
    /// </summary>
    public class SsmsColumn
    {
        /// <summary>
        /// Gets or sets the column name.
        /// </summary>
        /// <value>The name.</value>
        public string Name { get; set; }
        /// <summary>
        /// Gets or sets the number that is associated with a particular column, in the system registry.
        /// </summary>
        /// <value>The number.</value>
        public int Number { get; set; }
        /// <summary>
        /// Gets or sets the instance identifier. This makes it easy to determine which instance this column belongs to.
        /// </summary>
        /// <value>The instance identifier.</value>
        public Guid InstanceId { get; set; }
        /// <summary>
        /// Gets or sets a value indicating whether this instance is selected.
        /// </summary>
        /// <value><c>true</c> if this instance is selected; otherwise, <c>false</c>.</value>
        public bool IsSelected { get; set; }
    }
}